package com.keybank.cards.offers.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.keybank.cards.offers.beans.CreditLimitEnhanceResponse;
import com.keybank.cards.offers.exception.BusinessException;
import com.keybank.cards.offers.form.CleRequestForm;
import com.keybank.cards.offers.form.RedeemForm;
import com.keybank.cards.offers.service.CreditLimitEnhanceService;
import com.keybank.cards.offers.service.CreditLimitEnhnaceServiceImpl;

@Controller
public class CreditLimitEhanceController {
	CreditLimitEnhanceService cleService = new CreditLimitEnhnaceServiceImpl();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "crditlimitehnace";
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public ModelAndView enhanceLimit(@ModelAttribute CleRequestForm cleRequestForm) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		System.out.println(cleRequestForm.getPromocode());
		
		  try { CreditLimitEnhanceResponse creditLimitSvcResp =
	  cleService.verifyPromocode(cleRequestForm);
		  
		  // updated the customer details table
		  
		  // then return scces.jsp modelAndView.setViewName("success"); return
		    modelAndView.setViewName("redemption");
			modelAndView.addObject("svcRsp", creditLimitSvcResp);
			return modelAndView;
		  
		  } catch (BusinessException e) 
		  {
		  modelAndView.setViewName("error");
		  
		  System.out.println("in conntoller"+ e); modelAndView.addObject("error", e); }
		 

		return modelAndView;
	}
	
	@RequestMapping(value = "/reedeem", method = RequestMethod.POST)
	public ModelAndView redeemProcode(@ModelAttribute RedeemForm redeemForm) {
		System.out.println(redeemForm);
		//forward the request service layer
		//we call one more web service...............................
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		return modelAndView;}

	
	
	
}
