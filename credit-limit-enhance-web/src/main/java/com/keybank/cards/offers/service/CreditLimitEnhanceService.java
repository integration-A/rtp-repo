package com.keybank.cards.offers.service;

import com.keybank.cards.offers.beans.CreditLimitEnhanceResponse;
import com.keybank.cards.offers.exception.BusinessException;
import com.keybank.cards.offers.form.CleRequestForm;

public interface CreditLimitEnhanceService {
	
	public CreditLimitEnhanceResponse verifyPromocode(CleRequestForm cleRequestForm) throws BusinessException;

	

}
