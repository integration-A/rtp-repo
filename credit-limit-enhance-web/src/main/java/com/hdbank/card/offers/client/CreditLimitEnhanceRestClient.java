package com.hdbank.card.offers.client;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.keybank.cards.offers.beans.CreditLimitEnhanceResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class CreditLimitEnhanceRestClient {

	public static void main(String args[]) {

		/**
		 * we need to write the code that invoke the web service
		 */
		// first api calling for testing healthCheck
		Client cleRestClient = Client.create();
		String healthCheck = "http://localhost:8080/credit-limit-enhancemennt/api/cle-limit/health";
		WebResource webResource = cleRestClient.resource(healthCheck);
		String response = webResource.accept("text/html").get(String.class);
		System.out.println(response);

		// calling verify promocode api

		String verifyPromocodeUrl = "http://localhost:8080/credit-limit-enhancemennt/api/cle-limit/promocode";
		WebResource webResource2 = cleRestClient.resource(verifyPromocodeUrl);

		// Response ehanceResponse = webResource2.queryParam("promocode",
		// "hdbank101").accept(MediaType.APPLICATION_XML).get(Response.class);
		/*
		 * String ehanceResponse = webResource2.queryParam("promocode",
		 * "hdbank101").accept(MediaType.APPLICATION_JSON).get(String.class);
		 */

		CreditLimitEnhanceResponse ehanceResponse = webResource2.queryParam("promocode", "hdbank101")
				.accept(MediaType.APPLICATION_XML).get(CreditLimitEnhanceResponse.class);

		System.out.println(ehanceResponse);
		// String s=(String)ehanceResponse.getEntity();

	}

}
